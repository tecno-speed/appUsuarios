var colUsuarios;
var idUser = -1;
var db;

document.addEventListener('deviceready', onDeviceready)

function onDeviceready() {
    db = sqlitePlugin.openDatabase({ name: 'mBancoUsuarios.db' });

    db.transaction(function (txn) {
        // var sql = 'drop table usuarios'
        var sql = 'create table if not exists usuarios (id integer primary key, nome text, email text, sincronizado integer)'
        txn.executeSql(sql)

        localListar();
    });

}

function localListar() {
    colUsuarios = [];

    db.transaction(function (txn) {
        var sql = 'select * from usuarios'

        txn.executeSql(sql, [],
            (tx, onSuccess) => {

                for (var i = 0; i < onSuccess.rows.length; i++) {
                    colUsuarios.push(onSuccess.rows.item(i));
                }

            }, (tx, onError) => {
                console.log(tx, onError)
            });
    });

}

function localEditar(idUser, nome, email) {
    db.transaction(function (txn) {

        var sql = 'update usuarios set nome = ?, email = ? where (id = ' + idUser + ')'

        txn.executeSql(sql, [nome, email],
            (tx, onSuccess) => {

                localListar();
                closeLoading('btnSalvar')

                document.getElementById('nome').value = '';
                document.getElementById('email').value = '';

            }, (tx, onError) => {
                console.log(tx, onError)
            });
    });
}

function localInserir(nome, email) {
    db.transaction(function (txn) {

        var sql = 'insert into usuarios (nome, email) VALUES (?, ?)'

        txn.executeSql(sql, [nome, email],
            (tx, onSuccess) => {

                localListar();
                closeLoading('btnSalvar')

                document.getElementById('nome').value = '';
                document.getElementById('email').value = '';

            }, (tx, onError) => {
                console.log(tx, onError)
            });
    });
}

function salvar() {
    loadingElement('btnSalvar', 'Salvando...')

    var nome = document.getElementById('nome').value;
    var email = document.getElementById('email').value;

    if (idUser > 0) {
        localEditar(idUser, nome, email);
    } else {
        localInserir(nome, email);
    }

    idUser = -1;

}

function editar(id) {

    getByID(id).then((rows) => {

        idUser = rows['id'];
        document.getElementById('nome').value = rows['nome'];
        document.getElementById('email').value = rows['email'];

    });
}


function excluir(id) {
    db.transaction(function (txn) {
        var sql = 'delete from usuarios where (id = ?)'

        txn.executeSql(sql, [id],
            (tx, onSuccess) => {

                localListar();

            }, (tx, onError) => {
                console.log(tx, onError)
            });
    });
}


function getByID(id) {

    return new Promise((resolve, reject) => {

        db.transaction(function (txn) {
            var sql = 'select * from usuarios where (id = ?)'

            txn.executeSql(sql, [id],
                (tx, onSuccess) => {

                    resolve(onSuccess.rows.item(0))

                }, (tx, onError) => {
                    console.log(tx, onError)
                    reject(null);
                });
        });

    });
}

MobileUI.validarSincronizacao = function (sincronizado) {
    return !sincronizado ? '<span class="red radius padding">Não sincronizado</span>' : '<span class="green radius padding">OK</span>'
}

function job() {
    db.transaction(function (txn) {
        var sql = 'select * from usuarios where sincronizado is null limit 1'

        txn.executeSql(sql, [],
            (tx, onSuccess) => {

                if (onSuccess.rows.length) {

                    var usuario = onSuccess.rows.item(0)

                    /* salva online */
                    MobileUI.ajax
                        .post('http://localhost:3000/usuarios')
                        .send({
                            nome: usuario.nome,
                            email: usuario.email
                        })
                        .end(function (error, resAPI) {
                            if (error) {
                                alert('Ops, ocorreu um erro!')
                                return
                            }

                            /* Atualiza Local */
                            idUser = resAPI.body.insertId;
                            db.transaction(function (txn2) {

                                var sql = 'update usuarios set sincronizado = ? where (id = ?)'

                                txn2.executeSql(sql, [idUser, usuario.id],
                                    (tx2, onSuccess) => {

                                        localListar();

                                    }, (tx2, onErrorApi) => {
                                        console.log(tx2, onErrorApi)
                                    });
                            });

                        })

                }

            }, (tx, onError) => {
                console.log(tx, onError)
            });
    });

}

setInterval(function () { job() }, 10000);




// function listar() {
//     MobileUI.ajax
//         .get('http://localhost:3000/usuarios')
//         .end(function (error, res) {
//             if (error) {
//                 alert('Ops, ocorreu um erro!')
//                 return
//             }

//             colUsuarios = res.body;
//         })

// }

// listar();

// function salvar() {
//     loadingElement('btnSalvar', 'Salvando...')

//     var nome = document.getElementById('nome').value;
//     var email = document.getElementById('email').value;

//     if (idUser > 0) {
//         restEditar(idUser, nome, email);
//     } else {
//         restInserir(nome, email);
//     }

//     idUser = -1;

// }

// function restInserir(nome, email) {
//     MobileUI.ajax
//         .post('http://localhost:3000/usuarios')
//         .send({
//             nome: nome,
//             email: email
//         })
//         .end(function (error, res) {
//             if (error) {
//                 alert('Ops, ocorreu um erro!')
//                 return
//             }

//             alert('Item salvo com sucesso')

//             listar();
//             closeLoading('btnSalvar')

//     document.getElementById('nome').value = '';
//     document.getElementById('email').value = '';
//         })
// }

// function restEditar(idUser, nome, email) {
//     MobileUI.ajax
//         .put('http://localhost:3000/usuarios')
//         .send({
//             id: idUser,
//             nome: nome,
//             email: email
//         })
//         .end(function (error, res) {
//             if (error) {
//                 alert('Ops, ocorreu um erro!')
//                 return
//             }

//             alert('Item salvo com sucesso')
//             listar();
//             closeLoading('btnSalvar')

//     document.getElementById('nome').value = '';
//     document.getElementById('email').value = '';
//         })
// }

// function editar(id) {
//     idUser = id;

//     MobileUI.ajax
//         .get('http://localhost:3000/usuarios/' + id)
//         .end(function (error, res) {
//             if (error) {
//                 alert('Ops, ocorreu um erro!')
//                 return
//             }

//             document.getElementById('nome').value = res.body.nome;
//             document.getElementById('email').value = res.body.email;
//         })
// }

// function excluir(id) {
//     MobileUI.ajax
//         .delete('http://localhost:3000/usuarios/' + id)
//         .end(function (error, res) {
//             if (error) {
//                 alert('Ops, ocorreu um erro!')
//                 return
//             }

//             alert('Item removido com sucesso')
//             listar();
//         })
// }